var term=document.getElementById("term");
var cmd = "";

const builtins = [
	["help", "list available commands"],
	["ls", "list files"],
	["cat", "print file"]
]

const files = [
	["license", "BSD 2-Clause License<br><br>Copyright (c) 2021, binrc All rights reserved.<br><br>Redistribution and use in source and binary forms, with or without<br>modification, are permitted provided that the following conditions are met:<br><br>1. Redistributions of source code must retain the above copyright notice, this<br>&nbsp;&nbsp;&nbsp;list of conditions and the following disclaimer.<br><br>2. Redistributions in binary form must reproduce the above copyright notice,<br>&nbsp;&nbsp;&nbsp;this list of conditions and the following disclaimer in the documentation<br>&nbsp;&nbsp;&nbsp;and/or other materials provided with the distribution.<br><br>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"<br>AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE<br>IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE<br>DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE<br>FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL<br>DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR<br>SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER<br>CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,<br>OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE<br>OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."],
	["source", "You can find the source code <a href='https://gitlab.com/binrc/neocities'>here</a>"],
	["author", "<a href='http://0x19.org'>here<a>"],
	["stack", "# OS<br>&#9;<a href='http://getfedora.org'>For the tired</a><br>&#9;<a href='http://openbsd.org'>For the concerned</a><br>&#9;<a href='http://9front.org'>End of the line</a><br># Editor<br>&#9;<code> $ man vi </code><br>&#9;<code> $ man emacs </code><br># Languages<br>&#9;<a href='https://archive.org/details/TheCProgrammingLanguageFirstEdition'>C</a><br>&#9;<a href='https://pubs.opengroup.org/onlinepubs/9699919799/'>/bin/sh</a><br>&#9;<a href='https://www.php.net/'>PHP</a>"],
	["hardware", "# Hardware<br>## x220 <br><img src='assets/x220.png'><br>## t490 <br><img src='assets/t490.png'>"],
	["themes", "<a href='index.html'>Plan 9 theme</a><br><a href='vta.html'>VT Amber</a><br><a href='vtg.html'>VT Green</a>"]
]

function printl(buf){
	term.innerHTML = term.innerHTML + "<br>" + buf;
	scroll();
	return;
}

function help(){
	for(var i = 0; i<builtins.length; i++){
		printl(builtins[i][0] + " : " + builtins[i][1]);
	}
	return;
}

function ls(){
	for(var i = 0; i<files.length; i++){
		printl(files[i][0]);
	}
	return;
}

function cat(file){
	var hit = 0;
	for(var i = 0; i<files.length; i++){
		if(files[i][0] == file){
			printl(files[i][1]);
			hit = 1
		}
	}

	if(!hit)
		printl("no such file");

	scroll();
	return;
}

function exec(str){
	var hit = 0;
	tok = str.match(/\S+/g);
	for(var i = 0; i < builtins.length; i++){
		if(tok[0] == builtins[i][0]){
			funct = builtins[i][0]
			if(funct == "cat"){
				window[funct](tok[1]);
			} else {
				window[funct]();
			}
			hit = 1;
		}
	}
	window.cmd = "";
	if(!hit)
		printl("no such command");
	return;
}

function autoscroll(){
	term.scrollTop = term.scrollHeight;
	return;
}

function linefeed(){
	term.innerHTML = term.innerHTML + "<br>%&nbsp;";
}




function parser(key){
	autoscroll();
	switch(key){
		case "Enter":
			if(cmd.length > 0)
				exec(cmd);
			linefeed();
			autoscroll();
			break;
		case "Backspace":
			term.innerHTML = term.innerHTML.slice(0, -1);
			cmd = cmd.slice(0, -1);
			break;
		case "Shift":
		case "Control":
		case "Alt":
		case "Tab":
		case "CapsLock":
		case "Escape":
		case "Delete":
		case "Insert":
		case "Home":
		case "End":
		case "ArrowLeft":
		case "ArrowDown":
		case "ArrowUp":
		case "ArrowRight":
		case "PageUp":
		case "PageDown":
			break;
		default:
			term.innerHTML = term.innerHTML + key;
			cmd = cmd + key;
	}
	return;
}



window.addEventListener("keydown", function(e){
	if(e.defaultPrevented){
		return;
	}
	event.preventDefault();
	parser(e.key);
}, true);
term.innerHTML = "% help";
help();
linefeed();
